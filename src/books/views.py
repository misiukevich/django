from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView

from books.models import Book
from books.forms import BookCreateForm

from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class BookListView(ListView):
    model = Book
    template_name = 'books/list.html'


class BookCreateView(LoginRequiredMixin, CreateView):
    model = Book
    form_class = BookCreateForm
    success_url = '/books/list/'
    template_name = 'books/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Создание нового объекта'
        return context


class BookDetailView(DetailView):
    template_name = 'books/detail.html'
    model = Book


class BookUpdateView(LoginRequiredMixin, UpdateView):
    model = Book
    form_class = BookCreateForm
    success_url = '/books/list/'
    template_name = 'books/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Редактирование объекта'
        return context


class BookDeleteView(LoginRequiredMixin, DeleteView):
    model = Book
    success_url = '/books/list/'
    template_name = 'books/delete.html'
