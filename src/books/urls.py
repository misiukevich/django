from django.urls import path
from . import views

urlpatterns = [
    path('create/', views.BookCreateView.as_view()),
    path('<int:pk>/', views.BookDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', views.BookUpdateView.as_view()),
    path('delete/<int:pk>/', views.BookDeleteView.as_view()),
    path('list/', views.BookListView.as_view(), name='books-list'),

]