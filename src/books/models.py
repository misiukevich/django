from django.db import models
from catalogs.models import Genre, Author, PublHouse, Series

# Create your models here.


class Book(models.Model):
    genre = models.ForeignKey(
        Genre,
        related_name='books',
        on_delete=models.PROTECT,
        default=1
    )

    author = models.ForeignKey(
        Author,
        related_name='books',
        on_delete=models.PROTECT,
        default=1
    )

    publ_house = models.ForeignKey(
        PublHouse,
        related_name='books',
        on_delete=models.PROTECT,
        default=1
    )

    series = models.ForeignKey(
        Series,
        related_name='books',
        on_delete=models.PROTECT,
        default=1
    )

    name = models.CharField(max_length=100)

    foto = models.ImageField(
        upload_to='media',
        verbose_name='Картинка',
        null=True,
        blank=True,
        default='no foto'
    )

    price = models.FloatField(
        default=0,
        max_length=5
    )

    year_of_published = models.IntegerField(default=0)

    pages_number = models.IntegerField(default=0)

    hard = 'hrd'
    soft = 'sft'
    BINDING_CHOICES = [(hard, 'hard'), (soft, 'soft')]
    binding = models.CharField(
        max_length=3,
        choices=BINDING_CHOICES,
        default=hard
    )

    format_book = models.CharField(max_length=10)

    isbn = models.IntegerField(default=0)

    mass = models.FloatField(
        default=0,
        max_length=7
    )

    babies = '0+'
    teenagers = '12+'
    adults = '18+'
    AGE_CHOICES = [(babies, '0+'), (teenagers, '12+'), (adults, '18+')]
    age_restrictions = models.CharField(
        max_length=3,
        choices=AGE_CHOICES,
        default=babies
    )

    in_stock = models.IntegerField(default=0)

    positive = 'yes'
    negative = 'no'
    AVAILABLE_CHOICES = [(positive, 'yes'), (negative, 'no')]
    available = models.CharField(
        max_length=3,
        choices=AVAILABLE_CHOICES,
        default=negative
    )

    rating = models.IntegerField(default=0)

    created = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )

    updated = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )

    def __str__(self):
        return self.name
