from django.contrib import admin
from .models import Book

# Register your models here.


class BookAdmin(admin.ModelAdmin):
    list_display = ('name', 'foto', 'price', 'year_of_published', 'pages_number', 'binding', 'format_book',
                    'isbn', 'mass', 'age_restrictions', 'in_stock', 'available', 'rating', 'created', 'updated')


admin.site.register(Book, BookAdmin)
