from django.forms import ModelForm
from books.models import Book


class BookCreateForm(ModelForm):
    class Meta:
        model = Book
        fields = ['name',
                  'foto',
                  'price',
                  'year_of_published',
                  'pages_number',
                  'binding',
                  'format_book',
                  'isbn',
                  'mass',
                  'age_restrictions',
                  'in_stock',
                  'available',
                  'rating'
                  ]