from django.db import models
from django.shortcuts import reverse
from django.contrib.auth import get_user_model


User = get_user_model()


# Create your models here.


class UserAccount(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    password = models.CharField(max_length=20, blank=True)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField()

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse('user_account', kwargs={'user': self.user.username})
