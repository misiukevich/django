from django.forms import ModelForm
from .models import UserAccount
from django.contrib.auth import get_user_model

User = get_user_model()


class RegistrationForm (ModelForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'first_name',
            'last_name',
            'email'
        ]
