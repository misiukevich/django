from django.shortcuts import render, reverse
from django.views.generic.list import ListView
from books.models import Book
from django.views import View
from .forms import RegistrationForm
from .models import UserAccount
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect

User = get_user_model()

# Create your views here.


class HelloListView(ListView):
    model = Book
    limit = 6
    template_name = 'hello/hello.html'

    def get_queryset(self, **kwargs):
        qs = super().get_queryset(**kwargs)
        return qs[:self.limit]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_menu'] = 'Книги'
        return context


class RegistrationView(View):
    template_name = 'hello/registration.html'

    def get(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        context = {'form': form}
        return render(self.request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            new_user.set_password(password)
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            new_user.save()
            UserAccount.objects.create(user=User.objects.get(username=new_user.username),
                                       first_name=new_user.first_name,
                                       last_name=new_user.last_name,
                                       email=new_user.email
                                       )
            return HttpResponseRedirect('/')
        context = {'form': form}
        return render(self.request, self.template_name, context)


