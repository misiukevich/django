from django.urls import path
from . import views

urlpatterns = [

    path('', views.HelloListView.as_view(), name='hello'),
    path('registration/', views.RegistrationView.as_view(), name='registration')
]