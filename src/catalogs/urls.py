from django.urls import path
from . import views

urlpatterns = [
    path('author/create/', views.AuthorCreateView.as_view()),
    path('author/<int:pk>/', views.AuthorDetailView.as_view()),
    path('author/update/<int:pk>/', views.AuthorUpdateView.as_view()),
    path('author/delete/<int:pk>/', views.AuthorDeleteView.as_view()),
    path('author/list/', views.AuthorListView.as_view(), name='author-list'),
    path('series/create/', views.SeriesCreateView.as_view()),
    path('series/<int:pk>/', views.SeriesDetailView.as_view()),
    path('series/update/<int:pk>/', views.SeriesUpdateView.as_view()),
    path('series/delete/<int:pk>/', views.SeriesDeleteView.as_view()),
    path('series/list/', views.SeriesListView.as_view(), name='series-list'),
    path('genre/create/', views.GenreCreateView.as_view()),
    path('genre/<int:pk>/', views.GenreDetailView.as_view()),
    path('genre/update/<int:pk>/', views.GenreUpdateView.as_view()),
    path('genre/delete/<int:pk>/', views.GenreDeleteView.as_view()),
    path('genre/list/', views.GenreListView.as_view(), name='genre-list'),
    path('publhouse/create/', views.PublHouseCreateView.as_view()),
    path('publhouse/<int:pk>/', views.PublHouseDetailView.as_view()),
    path('publhouse/update/<int:pk>/', views.PublHouseUpdateView.as_view()),
    path('publhouse/delete/<int:pk>/', views.PublHouseDeleteView.as_view()),
    path('publhouse/list/', views.PublHouseListView.as_view(), name='publhouse-list')
]

