from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView

from catalogs.models import Author, Series, Genre, PublHouse
from catalogs.forms import AuthorCreateForm, SeriesCreateForm, GenreCreateForm, PublHouseCreateForm

from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.

#----------author----------


class AuthorListView(ListView):
    model = Author
    template_name = 'catalogs/author/list.html'


class AuthorCreateView(LoginRequiredMixin, CreateView):
    model = Author
    form_class = AuthorCreateForm
    success_url = '/author/list/'
    template_name = 'catalogs/author/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Создание нового объекта'
        return context


class AuthorDetailView(DetailView):
    template_name = 'catalogs/author/detail.html'
    model = Author


class AuthorUpdateView(LoginRequiredMixin, UpdateView):
    model = Author
    form_class = AuthorCreateForm
    success_url = '/author/list/'
    template_name = 'catalogs/author/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Редактирование объекта'
        return context


class AuthorDeleteView(LoginRequiredMixin, DeleteView):
    model = Author
    success_url = '/author/list/'
    template_name = 'catalogs/author/delete.html'


#----------series----------


class SeriesListView(ListView):
    model = Series
    template_name = 'catalogs/series/list.html'


class SeriesCreateView(LoginRequiredMixin, CreateView):
    model = Series
    form_class = SeriesCreateForm
    success_url = '/series/list/'
    template_name = 'catalogs/series/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Создание нового объекта'
        return context


class SeriesDetailView(DetailView):
    template_name = 'catalogs/series/detail.html'
    model = Series


class SeriesUpdateView(LoginRequiredMixin, UpdateView):
    model = Series
    form_class = SeriesCreateForm
    success_url = '/series/list/'
    template_name = 'catalogs/series/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Редактирование объекта'
        return context


class SeriesDeleteView(LoginRequiredMixin, DeleteView):
    model = Series
    success_url = '/series/list/'
    template_name = 'catalogs/series/delete.html'


#----------genre----------


class GenreListView(ListView):
    model = Genre
    template_name = 'catalogs/genre/list.html'


class GenreCreateView(LoginRequiredMixin, CreateView):
    model = Genre
    form_class = GenreCreateForm
    success_url = '/genre/list/'
    template_name = 'catalogs/genre/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Создание нового объекта'
        return context


class GenreDetailView(DetailView):
    template_name = 'catalogs/genre/detail.html'
    model = Genre


class GenreUpdateView(LoginRequiredMixin, UpdateView):
    model = Genre
    form_class = GenreCreateForm
    success_url = '/genre/list/'
    template_name = 'catalogs/genre/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Редактирование объекта'
        return context


class GenreDeleteView(LoginRequiredMixin, DeleteView):
    model = Genre
    success_url = '/genre/list/'
    template_name = 'catalogs/genre/delete.html'


#----------publhouse----------


class PublHouseListView(ListView):
    model = PublHouse
    template_name = 'catalogs/publhouse/list.html'


class PublHouseCreateView(LoginRequiredMixin, CreateView):
    model = PublHouse
    form_class = PublHouseCreateForm
    success_url = '/publhouse/list/'
    template_name = 'catalogs/publhouse/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Создание нового объекта'
        return context


class PublHouseDetailView(DetailView):
    template_name = 'catalogs/publhouse/detail.html'
    model = PublHouse


class PublHouseUpdateView(LoginRequiredMixin, UpdateView):
    model = PublHouse
    form_class = PublHouseCreateForm
    success_url = '/publhouse/list/'
    template_name = 'catalogs/publhouse/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Редактирование объекта'
        return context


class PublHouseDeleteView(LoginRequiredMixin, DeleteView):
    model = PublHouse
    success_url = '/publhouse/list/'
    template_name = 'catalogs/publhouse/delete.html'