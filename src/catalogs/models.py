from django.db import models


# Create your models here.


class Author(models.Model):
    name = models.CharField(
        max_length=100
    )

    description = models.TextField(
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name


class Series(models.Model):
    name = models.CharField(
        max_length=50
    )

    description = models.TextField(
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(
        max_length=15,
        blank=False,
        null=False
    )

    description = models.TextField(
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name


class PublHouse(models.Model):
    name = models.CharField(
        max_length=25
    )

    description = models.TextField(
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name
