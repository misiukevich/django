from django.forms import ModelForm
from catalogs.models import Author, Series, Genre, PublHouse


class AuthorCreateForm(ModelForm):
    class Meta:
        model = Author
        fields = ['name',
                  'description'
                  ]


class SeriesCreateForm(ModelForm):
    class Meta:
        model = Series
        fields = ['name',
                  'description'
                  ]


class GenreCreateForm(ModelForm):
    class Meta:
        model = Genre
        fields = ['name',
                  'description'
                  ]


class PublHouseCreateForm(ModelForm):
    class Meta:
        model = PublHouse
        fields = ['name',
                  'description'
                  ]

