from django.urls import path
from .views import AddToCartUpdateView
from .views import CartListView
from .views import BookInCartDeleteView

app_name = 'cart'
urlpatterns = [
    path('add/<int:pk>/', AddToCartUpdateView.as_view(), name='add'),
    path('cart/', CartListView.as_view(), name='cart'),
    path('cart/delete/<int:pk>/', BookInCartDeleteView.as_view(), name='delete')
]