from django.db import models
from django.contrib.auth import get_user_model
from books.models import Book

User = get_user_model()


class Cart(models.Model):
    user = models.ForeignKey(
        User,
        verbose_name='Клиент',
        on_delete=models.PROTECT,
        related_name="carts"
    )

    def total_price(self):
        books = self.books.all()
        total = 0
        for book in books:
            total += book.price
        return total


class BookInCart(models.Model):
    book = models.ForeignKey(
        Book,
        verbose_name="Книга",
        on_delete=models.PROTECT,
        related_name="books_in_cart"
    )
    cart = models.ForeignKey(
        Cart,
        verbose_name="Корзина",
        on_delete=models.PROTECT,
        related_name="books"
    )
    quantity = models.IntegerField(
        verbose_name="Количество",
        default=1
    )
    price = models.DecimalField(
        verbose_name="Цена",
        max_digits=8,
        decimal_places=2
    )

    def __str__(self):
        return f"{self.book.name} * {self.quantity}"

