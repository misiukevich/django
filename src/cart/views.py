from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from .models import BookInCart, Cart
from .forms import BookInCartForm
from books.models import Book
from django.urls import reverse_lazy


# Create your views here.


class AddToCartUpdateView(UpdateView):
    model = BookInCart
    form_class = BookInCartForm
    success_url = reverse_lazy('cart:cart')
    template_name = 'cart/add_to_cart.html'

    def form_valid(self, form,  *args, **kwargs):
        quantity = form.cleaned_data.get('quantity')
        price = self.object.book.price
        new_price = price * quantity
        self.object.price = new_price
        self.object.save()
        return super().form_valid(form)

    def get_object(self):
        book_to_add = self.kwargs.get('pk')
        # проверить существует ли книга
        cart_id = self.request.session.get('cart_id')
        if not cart_id:
            cart = Cart.objects.create(
                user=self.request.user
            )
            self.request.session['cart_id'] = cart.pk
        else:
            cart = Cart.objects.get(pk=cart_id)
        cart_id = self.request.session.get('cart_id')
        book = Book.objects.get(pk=book_to_add)
        book_in_cart, created = BookInCart.objects.get_or_create(
            book=book,
            cart=cart,
            defaults={'price': book.price}
        )
        if not created:
            book_in_cart.price = book.price * (book_in_cart.quantity + 1)
            book_in_cart.quantity = book_in_cart.quantity + 1
            book_in_cart.save()
        return book_in_cart

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CartListView(TemplateView):
    model = Cart
    template_name = 'cart/cart.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        cart_id = self.request.session.get('cart_id')
        cart = None
        if cart_id:
            cart = Cart.objects.get(pk=cart_id)
        context['cart'] = cart
        return context


class BookInCartDeleteView(DeleteView):
    model = BookInCart
    success_url = '/cart/'
    template_name = 'cart/delete.html'
