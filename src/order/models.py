from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
# Create your models here.


class Order(models.Model):
    cart = models.ForeignKey(
        'cart.Cart',
        verbose_name='Корзина',
        on_delete=models.PROTECT
    )
    price = models.DecimalField(
        verbose_name="Цена заказа",
        max_digits=8,
        decimal_places=2
    )
    delivery_price = models.DecimalField(
        verbose_name="Цена доставки",
        max_digits=8,
        decimal_places=2
    )
    delivery_address = models.TextField(
        'Адрес доставки',
        default=" "
    )
    status = models.BooleanField(
          'Исполнен ',
          default=False
    )


@receiver(post_save, sender=Order)
def order_updater(sender, **kwargs):
    print(type(sender), sender)
