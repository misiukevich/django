from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView
from .models import Order
from .forms import OrderCreateForm
from cart.models import Cart
from .utils import get_price
# Create your views here.


class ThanksView(TemplateView):
    model = Order
    template_name = 'order/thanks.html'


class OrderCreate(UpdateView):
    template_name = 'order/create.html'
    model = Order
    form_class = OrderCreateForm
    success_url = 'thanks/'

    def get_object(self):
        order_id = self.request.session.get('order_id')
        cart_id = self.request.session.get('cart_id')

        cart = Cart.objects.get(pk=cart_id)
        order, created = Order.objects.get_or_create(
                cart=cart,
                price=get_price(cart.total_price())[0],
                delivery_price=get_price(cart.total_price())[1],
            )
        if created:
            self.request.session['order_id'] = order.pk
        return order

    # def get_success_url(self):
    #     self.request.session.get['order_id'] = None
    #     self.request.session.get['cart_id'] = None
    #     return super().get_success_url()
