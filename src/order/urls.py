from django.urls import path
from .views import *

app_name = 'order'

urlpatterns = [
    path('checkout/', OrderCreate.as_view(), name='create'),
    path('checkout/thanks/', ThanksView.as_view())


]



